# Comix for Cata
This is the backport of funky addon "Comix" (v6.2.3.0) for Cataclysm 4.3.4

## How to install:
1. [Download the package](https://gitlab.com/jiliby/comix-cata/-/archive/1.0/comix-cata-1.0.zip).
2. Open the Zip package inside which you will find a single folder named `Comix`.
3. Extract or drag and drop the unique folder `Comix` into your `%WoW_Directory%\Interface\AddOns` folder.
